import datetime
import time

from selenium import webdriver

from secret import password, username

"""
Not going to be putting almost any comments
in here because I am just practicing actually
using oop in Python
"""


class tera_scrape:
    exe_path = "C:\Program Files (x86)\chromedriver.exe"
    prefs = {
        "download.default_directory": r"C:\Users\Hank\Documents\Testom\token_details"
    }
    options = webdriver.ChromeOptions()
    options.headless = True
    options.add_argument("--log-level=3")
    options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(exe_path, options=options)

    url = "https://gdchillers.teraportal.com/tadmin/minister/prepaids/advanced-search"
    print(url)
    driver.get(url)
    uid = username
    pwd = password

    @property
    def tera(self):
        try:
            user = self.driver.find_element_by_id("login_mail")
            password = self.driver.find_element_by_id("login_pass")
            user.clear()
            password.clear()
            user.send_keys(self.uid)
            password.send_keys(self.pwd)
            time.sleep(10)
            self.driver.find_element_by_name("submit").click()
            time.sleep(10)
            print("... We're in.")
            time.sleep(10)
            self.driver.find_element_by_id("btn-download").click()
            time.sleep(25)
            print("Download complete!")
        except Exception as e:
            print(f"This is what is happening: \n {e}")


ts = tera_scrape()
ts.tera
