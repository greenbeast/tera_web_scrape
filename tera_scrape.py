import datetime
import time

from selenium import webdriver

from secret import (  # This is where the username and password info are stored.
    password, username)

# Need to download the browser driver online @ https://sites.google.com/a/chromium.org/chromedriver/downloads
exe_path = "C:\Program Files (x86)\chromedriver.exe"

# This changes the default directory that the file is saved to
prefs = {"download.default_directory": r"C:\Users\Hank\Documents\Testom\token_details"}
options = (
    webdriver.ChromeOptions()
)  # Changed chrome_options to options because chrome_options is depreciated.

# chrome_options.add_argument("--headless")#makes it so that the browser doesn't actually open
options.headless = True  # this also makes it work without opening the browser

options.add_argument("--log-level=3")  # This should get rid of a depreciation error.

options.add_experimental_option("prefs", prefs)
# applies the above changes to the webdriver
driver = webdriver.Chrome(exe_path, options=options)

url = "https://gdchillers.teraportal.com/tadmin/minister/prepaids/advanced-search"
print(url)
driver.get(url)
uid = username
pwd = password

# bunch of time.sleep() in here because we have to give time for pages to load and whatnot
def tera():
    try:
        # Use inspect element to find login and password text box names
        user = driver.find_element_by_id("login_mail")
        password = driver.find_element_by_id("login_pass")
        # Clears the text boxes just in case
        user.clear()
        password.clear()
        # Inputs the username and password
        user.send_keys(uid)
        password.send_keys(pwd)
        time.sleep(5)
        # This is the button for login
        driver.find_element_by_name("submit").click()
        time.sleep(15)
        print("... We're in.")
        time.sleep(10)
        # This is the download button that gets clicked on
        driver.find_element_by_id("btn-download").click()
        time.sleep(20)
        print("Download Complete!")
    except Exception as e:
        print(f"This is what is happening: \n {e}")


tera()
